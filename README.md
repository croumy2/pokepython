# POKEPYTHON
# Clemence Roumy - Baptiste Fauvel
Pokepython est un jeu Pokémon réaliser avec le framework python, Django.

L'objectif du jeu consiste, comme dans tout Pokémon, à capturer des Pokémon afin de les utiliser dans des combats contre les meilleurs dresseurs de la région.

# MENU

### Login :
Insérer votre pseudo de joueur afin d'accèder au jeu. Un pseudo ne peut être utilisé qu'une seule fois.

### Home :
Sur cet écran, vous avez accès à votre pseudo, les Pokémons présent dans votre équipe, les Pokémon capturés stockés dans votre PC  ainsi que la possibilité de (peut-être) lancer un combat ou capturer un nouveau Pokémon.

### Mon PC :
Vous voyez sur cette page tous les Pokémon que vous avez capturé.

### Capture : 
Vous avez accès à la capture en cliquant sur "Combat" sur la page d'accueil. Vous devez ensuite choisir la Pokéball que vous souhaitez lancer puis cliquer sur "Capturer". Vous avez égalemment la possibilité de fuir le combat. Vous arrivez ensuite sur une page vous indiquant le succès (ou non de votre capture).

### Inventaire : 
Vous avez ici accès à votre stock de pokéball. Vous pouvez également acheté des Pokéball avec la Poké Moneyyy que vous avez durement gagné.

### Combat :
Combat contre des dresseurs.

# TO DO

### Pokédex

- [x] Affichage des Pokémons (non-vu = placeholder; vu & non-capturé = pokémon grisé; vu & capturé = pokémon)
- [x] Détail d'un Pokémon
- [x] Affichage du PC

### Capture de Pokémon

- [x] Capture d'un pokemon
- [x] Choix d'une Pokéball
- [x] Pourcentage de réussite en fonction de la Pokéball choisie
- [x] Stock de Pokéball
- [ ] Plusieurs essais lors d'une capture

### Inventaire
- [x] Pokéball à disposition
- [x] Magasin pour l'achat de Pokéball
- [x] Argent

### Combat 

Pas eu assez de temps pour réaliser cette feature

- [ ] .......

