from django.urls import path

from . import views

urlpatterns = [
    path('', views.login, name='login'),
    path('home', views.home, name='home'),
    path('fight', views.fight, name='fight'),
    path('pc', views.pc, name='pc'),
    path('inventory', views.inventory, name='inventory'),
    path('shop', views.shop, name='shop'),
    path('pokedex', views.pokedex, name='pokedex'),
    path('pokemon/<int:id>', views.pokemon, name='pokemon'),
    path('catch', views.catch, name='catch'),
]