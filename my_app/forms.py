from django import forms


class LoginForm(forms.Form):

    nickname = forms.CharField(required=True, label='', widget=forms.TextInput(attrs={'placeholder': 'Pseudo', 'class':'col-sm-3 form-control'}), max_length=50)
