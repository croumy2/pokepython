from django.db import models


# Create your models here.

class Pokeball(models.Model):
    idApi = models.IntegerField()
    pourcentage = models.DecimalField(decimal_places=1,max_digits=3)

    def __str__(self):
        return str(self.idApi)

class Trainer(models.Model):
    nickname = models.CharField(max_length=50, unique=True)
    money = models.DecimalField(max_digits=10,decimal_places=2)
    pokeball = models.ManyToManyField(Pokeball,through='Inventory')
    #poke = models.IntegerField()
    #great = models.IntegerField()
    #ultra = models.IntegerField()

    def __str__(self):
        return self.nickname

class Inventory(models.Model):
    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE)
    pokeball = models.ForeignKey(Pokeball, on_delete=models.CASCADE)
    quantity = models.IntegerField()

class Pokemon(models.Model):
    idPokedex = models.IntegerField()
    lp = models.IntegerField()
    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

class Pokedex(models.Model):
    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE)
    pokemon = models.IntegerField()
    seen = models.BooleanField()
    captured = models.BooleanField()

    def __str__(self):
        return str(self.id)