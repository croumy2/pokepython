from django.contrib import admin

# Register your models here.
from .models import Trainer, Inventory
from .models import Pokemon
from .models import Pokeball
from .models import Pokedex

admin.site.register(Trainer)
admin.site.register(Pokemon)
admin.site.register(Pokeball)
admin.site.register(Inventory)
admin.site.register(Pokedex)