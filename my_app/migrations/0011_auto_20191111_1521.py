# Generated by Django 2.2.6 on 2019-11-11 15:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('my_app', '0010_auto_20191111_1502'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='trainer',
            name='great',
        ),
        migrations.RemoveField(
            model_name='trainer',
            name='poke',
        ),
        migrations.RemoveField(
            model_name='trainer',
            name='ultra',
        ),
        migrations.CreateModel(
            name='Inventory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('pokeball', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_app.Pokeball')),
                ('trainer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_app.Trainer')),
            ],
        ),
        migrations.AddField(
            model_name='trainer',
            name='pokeball',
            field=models.ManyToManyField(through='my_app.Inventory', to='my_app.Pokeball'),
        ),
    ]
