import decimal
import random
from django.db import IntegrityError

from django.http import HttpResponseRedirect
from django.shortcuts import render
import my_app.helpers.api as api
from my_app.helpers.items.Pokemon import Pokemon

# Create your views here.
from my_app.models import Trainer, Pokemon, Pokeball, Inventory, Pokedex
from .forms import LoginForm


def login(request):
    error = None

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            nickname = request.POST.get('nickname')
            starter = random.randrange(1, 8, 3)

            try:
                trainer = Trainer(nickname=nickname, money=50000)
                trainer.save()

                initPokedex(trainer)
                Pokedex.objects.filter(trainer=trainer, pokemon=starter).update(seen=True, captured=True)

                pokemon = Pokemon(idPokedex=starter, lp=100, trainer=trainer)
                pokemon.save()

                balls = Pokeball.objects.all()

                for pokeball in balls:
                    inventory = Inventory(trainer=trainer, pokeball=pokeball, quantity=100 - pokeball.idApi * 10)
                    inventory.save()

                request.session['nickname'] = nickname

                return HttpResponseRedirect('/home')

            except IntegrityError:
                trainer = Trainer.objects.get(nickname=nickname)
                request.session['nickname'] = nickname
                return HttpResponseRedirect('/home')

            except:
                error = "Erreur, réessayez"

    else:
        form = LoginForm()

    return render(request, 'my_app/login.html', {'form': form, 'error': error if error is not None else ''})

def initPokedex(trainer):
    for index in range(1,152):
        pokedexModel = Pokedex(trainer=trainer, pokemon=index, seen=False, captured=False)
        pokedexModel.save()

def home(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)
    #Get Team from DB
    team = Pokemon.objects.filter(trainer=trainer)[:6]

    #Fill array with value from POKEAPI
    team = map(lambda pokemon: api.getPokemonById(request, pokemon.idPokedex),team)

    return render(request, 'my_app/home.html', {'trainer': trainer, 'team': team})

def fight(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    index = random.randrange(1, 152)
    pokemon = api.getPokemonById(request, index)
    Pokedex.objects.filter(trainer=trainer, pokemon=index).update(seen=True)

    pokeballs = []
    for ball in Inventory.objects.filter(trainer=trainer):
        pokeballs.append({'ball': api.getPokeballById(ball.pokeball, 0), 'quantity': ball.quantity})

    return render(request, 'my_app/fight.html', {'pokemon': pokemon, 'pokeballs': pokeballs})


def catch(request):
    message = ""

    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    index = request.POST.get('pokemon')
    pokemon = api.getPokemonById(request, index)

    ballId = request.POST.get('pokeball')
    pokeball = Pokeball.objects.get(idApi=ballId)

    inventoryBall = Inventory.objects.get(trainer=trainer, pokeball=pokeball)
    inventoryBall.quantity -= 1
    inventoryBall.save()

    pourcentage = pokeball.pourcentage
    chance = random.random()

    if (chance <= pourcentage):
        pokemonModel = Pokemon(idPokedex=index, lp=100, trainer=trainer)
        pokemonModel.save()
        message = "Bravo! Pokémon capturé"
        Pokedex.objects.filter(trainer=trainer, pokemon=index).update(captured=True)
    else:
       message = "Oh non ! Vous n'avez pas attrapé ce Pokémon"

    return render(request, 'my_app/catch.html', {'pokemon': pokemon, 'message': message})


def pc(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)
    # Get Team from DB
    team = Pokemon.objects.filter(trainer=trainer)[6:]

    # Fill array with value from POKEAPI
    team = map(lambda pokemon: api.getPokemonById(request, pokemon.idPokedex), team)

    return render(request, 'my_app/pc.html', {'team': team})


def inventory(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    balls = []
    for ball in Inventory.objects.filter(trainer=trainer):
        balls.append({'ball': api.getPokeballById(ball.pokeball, 0), 'quantity': ball.quantity})

    return render(request, 'my_app/inventory.html', {'trainer': trainer, 'balls': balls})

def shop(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    message = ""

    balls = []
    for ball in Pokeball.objects.all():
        balls.append(api.getPokeballById(ball.idApi, 0))

    if request.method == 'POST':
        pokeball = int(request.POST.get('number-2')) * decimal.Decimal(request.POST.get('price-2'))
        greatball = int(request.POST.get('number-3')) * decimal.Decimal(request.POST.get('price-3'))
        ultraball = int(request.POST.get('number-4')) * decimal.Decimal(request.POST.get('price-4'))

        pokeballInventory = Inventory.objects.get(trainer=trainer, pokeball=Pokeball.objects.get(idApi=2))
        greatballInventory = Inventory.objects.get(trainer=trainer, pokeball=Pokeball.objects.get(idApi=3))
        ultraballInventory = Inventory.objects.get(trainer=trainer, pokeball=Pokeball.objects.get(idApi=4))

        total = pokeball + greatball + ultraball

        pokeballInventory.quantity += int(request.POST.get('number-2'))
        pokeballInventory.save()

        greatballInventory.quantity += int(request.POST.get('number-3'))
        greatballInventory.save()

        ultraballInventory.quantity += int(request.POST.get('number-4'))
        ultraballInventory.save()

        cost = trainer.money - total

        if cost < 0:
            message = "Oups, vous n'avez pas assez d'argent"
        else:
            trainer.money -= total
            trainer.save()
            return HttpResponseRedirect('/inventory')

    return render(request, 'my_app/shop.html', {"trainer": trainer,"balls": balls, "message": message})


def pokedex(request):
    template = 'my_app/pokedex.html'
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)
    data = Pokedex.objects.filter(trainer=trainer)
    pokemons = []
    pokemonsList = api.getPokemonsList(request)
    for pokemon in pokemonsList:
        pokemons.append(pokemon)
    args = {}
    args['pokemons'] = pokemons
    args['pokedex'] = data
    return render(request, template, args)


def pokemon(request, id):
    template = 'my_app/pokemon.html'
    data = api.getPokemonById(request, id)
    args = {}
    args['pokemon'] = data
    return render(request, template, args)
