class Pokeball:
    name = ""
    apiID = 0
    pourcentage = 1
    price = 0
    picture = ""

    def __init__(self, pourcentage, pokeballData):
        self.name = pokeballData['name']
        self.pourcentage = pourcentage
        self.apiID = pokeballData["id"]
        self.price = pokeballData['cost']
        self.picture = pokeballData['sprites']['default']

