class Pokemon:
    def __init__(self, name, frontPicture, number, seen, captured, description = None):
        self.name = name
        self.frontPicture = frontPicture
        self.number = number
        self.seen = seen
        self.captured = captured
        self.description = description
