import requests
import json

from my_app.helpers.items.Pokeball import Pokeball
from my_app.helpers.items.Pokemon import Pokemon
from my_app.models import Trainer, Pokedex


def getPokemonsList(request):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    r = requests.get("https://pokeapi.co/api/v2/pokedex/2/")
    data = r.json()['pokemon_entries']
    pokemons = []
    for pokemon in data:
        pokedex = Pokedex.objects.filter(trainer=trainer, pokemon=pokemon['entry_number'])
        pokemonData = getPokemonData(pokemon['entry_number'])

        for field in pokemonData['names']:
            if (field['language']['name'] == 'fr'):
                name = field['name']

        for field in pokemonData['flavor_text_entries']:
            if (field['language']['name'] == 'fr'):
                description = field['flavor_text']

        # name = getPokemonFrName(pokemonData)
        # description = getPokemonFrData(pokemonData)


        pokemons.append(Pokemon(name,
                                'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' + str(
                                    pokemon['entry_number']) + '.png',
                                int(pokemon['entry_number']),
                                pokedex[0].seen,
                                pokedex[0].captured,
                                description
                                ))

    return pokemons


def getPokemonById(request, id):
    nicknameSession = request.session.get('nickname')
    trainer = Trainer.objects.get(nickname=nicknameSession)

    r = requests.get("https://pokeapi.co/api/v2/pokemon/" + str(id))
    pokemonData = r.json()
    pokemonInfo = getPokemonData(id)

    for field in pokemonInfo['names']:
        if (field['language']['name'] == 'fr'):
            name = field['name']

    for field in pokemonInfo['flavor_text_entries']:
        if (field['language']['name'] == 'fr'):
            description = field['flavor_text']

    pokedex = Pokedex.objects.filter(trainer=trainer, pokemon=id)

    pokemon = Pokemon(name, pokemonData["sprites"]["front_default"], int(pokemonData["id"]), pokedex[0].seen,
                      pokedex[0].captured, description)
    return pokemon


def getPokeballById(id, pourcentage):
    r = requests.get("https://pokeapi.co/api/v2/item/" + str(id))
    pokeballData = r.json()
    pokeball = Pokeball(pourcentage, pokeballData)

    return pokeball

# def getPokemonFrData(data):
#     for field in data:
#         if (field['language']['name'] == 'fr'):
#             return field
#
#
# def getPokemonFrName(data):
#     for field in data:
#         if (field['language']['name'] == 'fr'):
#             return field

def getPokemonData(id):
    r = requests.get("https://pokeapi.co/api/v2/pokemon-species/" + str(id))
    pokemonData = r.json()

    return pokemonData